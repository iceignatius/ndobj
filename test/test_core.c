#include <stddef.h>
#include <stdint.h>
#include <stdarg.h>
#include <setjmp.h>
#include <string.h>
#include <cmocka.h>
#include "ndobj.h"
#include "test_core.h"

static
void length_field_test(void **state)
{
    {
        static const size_t target = 0;
        static const uint8_t bindata[] = { 0x00 };

        uint8_t buf[512] = {0};
        assert_int_equal(ndobj_length_encode(NULL, 0, target), sizeof(bindata));
        assert_int_equal(ndobj_length_encode(buf, sizeof(bindata)-1, target), 0);
        assert_int_equal(ndobj_length_encode(buf, sizeof(buf), target), sizeof(bindata));
        assert_memory_equal(buf, bindata, sizeof(bindata));

        size_t value = 0;
        assert_int_equal(ndobj_length_decode(bindata, sizeof(bindata), &value), sizeof(bindata));
        assert_int_equal(value, target);
    }

    {
        static const size_t target = 70;
        static const uint8_t bindata[] = { 0x46 };

        uint8_t buf[512] = {0};
        assert_int_equal(ndobj_length_encode(NULL, 0, target), sizeof(bindata));
        assert_int_equal(ndobj_length_encode(buf, sizeof(bindata)-1, target), 0);
        assert_int_equal(ndobj_length_encode(buf, sizeof(buf), target), sizeof(bindata));
        assert_memory_equal(buf, bindata, sizeof(bindata));

        size_t value = 0;
        assert_int_equal(ndobj_length_decode(bindata, sizeof(bindata), &value), sizeof(bindata));
        assert_int_equal(value, target);
    }

    {
        static const size_t target = 7000;
        static const uint8_t bindata[] = { 0x82, 0x1B, 0x58 };

        uint8_t buf[512] = {0};
        assert_int_equal(ndobj_length_encode(NULL, 0, target), sizeof(bindata));
        assert_int_equal(ndobj_length_encode(buf, sizeof(bindata)-1, target), 0);
        assert_int_equal(ndobj_length_encode(buf, sizeof(buf), target), sizeof(bindata));
        assert_memory_equal(buf, bindata, sizeof(bindata));

        size_t value = 0;
        assert_int_equal(ndobj_length_decode(bindata, sizeof(bindata), &value), sizeof(bindata));
        assert_int_equal(value, target);
    }

    {
        static const size_t target = 70006000ULL;
        static const uint8_t bindata[] = { 0x84, 0x04, 0x2C, 0x34, 0xF0 };

        uint8_t buf[512] = {0};
        assert_int_equal(ndobj_length_encode(NULL, 0, target), sizeof(bindata));
        assert_int_equal(ndobj_length_encode(buf, sizeof(bindata)-1, target), 0);
        assert_int_equal(ndobj_length_encode(buf, sizeof(buf), target), sizeof(bindata));
        assert_memory_equal(buf, bindata, sizeof(bindata));

        size_t value = 0;
        assert_int_equal(ndobj_length_decode(bindata, sizeof(bindata), &value), sizeof(bindata));
        assert_int_equal(value, target);
    }

    {
        static const size_t target = 700060005000ULL;
        static const uint8_t bindata[] = { 0x88, 0x00, 0x00, 0x00, 0xA2, 0xFE, 0xD3, 0xF2, 0x88 };

        uint8_t buf[512] = {0};
        assert_int_equal(ndobj_length_encode(NULL, 0, target), sizeof(bindata));
        assert_int_equal(ndobj_length_encode(buf, sizeof(bindata)-1, target), 0);
        assert_int_equal(ndobj_length_encode(buf, sizeof(buf), target), sizeof(bindata));
        assert_memory_equal(buf, bindata, sizeof(bindata));

        size_t value = 0;
        assert_int_equal(ndobj_length_decode(bindata, sizeof(bindata), &value), sizeof(bindata));
        assert_int_equal(value, target);
    }
}

static
void header_encode_test(void **state)
{
    static const uint8_t bindata[] =
    {
        't', 'e', 's', 't', '-', 't', 'a', 'g',
        0x80 | 'S',
        5,
    };

    assert_int_equal(
        ndobj_header_encode(
            NULL,
            0,
            "test-tag",
            NDOBJ_TYPE_STRING,
            strlen("value")),
        sizeof(bindata));

    uint8_t buf[512] = {0};

    assert_int_equal(
        ndobj_header_encode(buf,
            sizeof(bindata) - 1,
            "test-tag",
            NDOBJ_TYPE_STRING,
            strlen("value")),
        0);

    assert_int_equal(
        ndobj_header_encode(
            buf,
            sizeof(buf),
            "test-tag",
            NDOBJ_TYPE_STRING,
            strlen("value")),
        sizeof(bindata));

    assert_memory_equal(buf, bindata, sizeof(bindata));
}

static
void unit_encode_test(void **state)
{
    static const uint8_t bindata[] =
    {
        't', 'e', 's', 't', '-', 't', 'a', 'g',
        0x80 | 'S',
        5,
        'v', 'a', 'l', 'u', 'e',
    };

    assert_int_equal(
        ndobj_unit_encode(
            NULL,
            0,
            "test-tag",
            NDOBJ_TYPE_STRING,
            "value",
            strlen("value")),
        sizeof(bindata));

    uint8_t buf[512] = {0};

    assert_int_equal(
        ndobj_unit_encode(
            buf,
            sizeof(bindata) - 1,
            "test-tag",
            NDOBJ_TYPE_STRING,
            "value",
            strlen("value")),
        0);

    assert_int_equal(
        ndobj_unit_encode(
            buf,
            sizeof(buf),
            "test-tag",
            NDOBJ_TYPE_STRING,
            "value",
            strlen("value")),
        sizeof(bindata));

    assert_memory_equal(buf, bindata, sizeof(bindata));
}

static
void unit_decode_test(void **state)
{
    static const uint8_t bindata[] =
    {
        't', 'e', 's', 't', '-', 't', 'a', 'g',
        0x80 | 'S',
        5,
        'v', 'a', 'l', 'u', 'e',
    };

    const char  *name = NULL;
    size_t      name_len = 0;
    int         type = 0;
    const void  *body = NULL;
    size_t      body_size = 0;

    assert_int_equal(
        ndobj_unit_decode(bindata, sizeof(bindata), &name, &name_len, &type, &body, &body_size),
        sizeof(bindata));

    assert_int_equal(name_len, strlen("test-tag"));
    assert_memory_equal(name, "test-tag", strlen("test-tag"));
    assert_int_equal(type, NDOBJ_TYPE_STRING);
    assert_int_equal(body_size, strlen("value"));
    assert_memory_equal(body, "value", strlen("value"));
}

int test_core(void)
{
    struct CMUnitTest tests[] =
    {
        cmocka_unit_test(length_field_test),
        cmocka_unit_test(header_encode_test),
        cmocka_unit_test(unit_encode_test),
        cmocka_unit_test(unit_decode_test),
    };

    return cmocka_run_group_tests_name("Core Functions Test", tests, NULL, NULL);
}
