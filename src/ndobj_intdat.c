#include <string.h>
#include <endian.h>
#include "ndobj_intdat.h"

void ndobj_intdat_write_uint16be(uint8_t *buf, uint16_t value)
{
    union
    {
        uint16_t value;
        uint8_t bytes[2];
    } exg;

    exg.value = htobe16(value);
    memcpy(buf, exg.bytes, sizeof(exg.bytes));
}

void ndobj_intdat_write_uint32be(uint8_t *buf, uint32_t value)
{
    union
    {
        uint32_t value;
        uint8_t bytes[4];
    } exg;

    exg.value = htobe32(value);
    memcpy(buf, exg.bytes, sizeof(exg.bytes));
}

void ndobj_intdat_write_uint64be(uint8_t *buf, uint64_t value)
{
    union
    {
        uint64_t value;
        uint8_t bytes[8];
    } exg;

    exg.value = htobe64(value);
    memcpy(buf, exg.bytes, sizeof(exg.bytes));
}

void ndobj_intdat_write_sint16be(uint8_t *buf, int16_t value)
{
    union
    {
        int16_t     sval;
        uint16_t    uval;
        uint8_t     bytes[2];
    } exg;

    exg.sval = value;
    exg.uval = htobe16(exg.uval);
    memcpy(buf, exg.bytes, sizeof(exg.bytes));
}

void ndobj_intdat_write_sint32be(uint8_t *buf, int32_t value)
{
    union
    {
        int32_t     sval;
        uint32_t    uval;
        uint8_t     bytes[4];
    } exg;

    exg.sval = value;
    exg.uval = htobe32(exg.uval);
    memcpy(buf, exg.bytes, sizeof(exg.bytes));
}

void ndobj_intdat_write_sint64be(uint8_t *buf, int64_t value)
{
    union
    {
        int64_t     sval;
        uint64_t    uval;
        uint8_t     bytes[8];
    } exg;

    exg.sval = value;
    exg.uval = htobe64(exg.uval);
    memcpy(buf, exg.bytes, sizeof(exg.bytes));
}

uint16_t ndobj_intdat_read_uint16be(const uint8_t *src)
{
    union
    {
        uint16_t value;
        uint8_t bytes[2];
    } exg;

    memcpy(exg.bytes, src, sizeof(exg.bytes));
    return be16toh(exg.value);
}

uint32_t ndobj_intdat_read_uint32be(const uint8_t *src)
{
    union
    {
        uint32_t value;
        uint8_t bytes[4];
    } exg;

    memcpy(exg.bytes, src, sizeof(exg.bytes));
    return be32toh(exg.value);
}

uint64_t ndobj_intdat_read_uint64be(const uint8_t *src)
{
    union
    {
        uint64_t value;
        uint8_t bytes[8];
    } exg;

    memcpy(exg.bytes, src, sizeof(exg.bytes));
    return be64toh(exg.value);
}

int16_t ndobj_intdat_read_sint16be(const uint8_t *src)
{
    union
    {
        int16_t     sval;
        uint16_t    uval;
        uint8_t     bytes[2];
    } exg;

    memcpy(exg.bytes, src, sizeof(exg.bytes));
    exg.uval = be16toh(exg.uval);

    return exg.sval;
}

int32_t ndobj_intdat_read_sint32be(const uint8_t *src)
{
    union
    {
        int32_t     sval;
        uint32_t    uval;
        uint8_t     bytes[4];
    } exg;

    memcpy(exg.bytes, src, sizeof(exg.bytes));
    exg.uval = be32toh(exg.uval);

    return exg.sval;
}

int64_t ndobj_intdat_read_sint64be(const uint8_t *src)
{
    union
    {
        int64_t     sval;
        uint64_t    uval;
        uint8_t     bytes[8];
    } exg;

    memcpy(exg.bytes, src, sizeof(exg.bytes));
    exg.uval = be64toh(exg.uval);

    return exg.sval;
}
