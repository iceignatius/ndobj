#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "ndobj_core.h"
#include "ndobj_typedef.h"
#include "ndobj_ele_list.h"

static const struct ndobj_ele_vtbl vtbl =
{
    .release = (void(*)(ndobj_ele_t*)) ndobj_ele_list_release,

    .get_name = (const char*(*)(const ndobj_ele_t*))
        ndobj_ele_list_get_name,

    .get_type = (int(*)(const ndobj_ele_t*))
        ndobj_ele_list_get_type,

    .count_children = (unsigned(*)(const ndobj_ele_t*))
        ndobj_ele_list_count_children,

    .add_child = (bool(*)(ndobj_ele_t*,ndobj_ele_t*))
        ndobj_ele_list_add_child,

    .get_next_child =
        (const ndobj_ele_t*(*)(const ndobj_ele_t*,const ndobj_ele_t*,const char*))
        ndobj_ele_list_get_next_child,

    .get_prev_child =
        (const ndobj_ele_t*(*)(const ndobj_ele_t*,const ndobj_ele_t*,const char*))
        ndobj_ele_list_get_prev_child,

    .encode = (size_t(*)(const ndobj_ele_t*,void*,size_t))
        ndobj_ele_list_encode,
};

ndobj_ele_list_t* ndobj_ele_list_create(const char *name)
{
    ndobj_ele_list_t *inst = NULL;

    bool succ = false;
    do
    {
        inst = malloc(sizeof(*inst));
        if( !inst ) break;

        ndobj_ele_init(&inst->super, &vtbl);

        inst->name = NULL;

        inst->count = 0;
        inst->head  = NULL;
        inst->tail  = NULL;

        inst->name = strdup(name);
        if( !inst->name ) break;

        succ = true;
    } while(false);

    if( !succ && inst )
    {
        ndobj_ele_list_release(inst);
        inst = NULL;
    }

    return inst;
}

void ndobj_ele_list_release(ndobj_ele_list_t *self)
{
    ndobj_ele_t *item = self->head;
    while( item )
    {
        ndobj_ele_t *worker = item;
        item = item->next;

        ndobj_ele_release(worker);
    }

    if( self->name )
        free(self->name);

    free(self);
}

const char* ndobj_ele_list_get_name(const ndobj_ele_list_t *self)
{
    return self->name;
}

int ndobj_ele_list_get_type(const ndobj_ele_list_t *self)
{
    return NDOBJ_TYPE_LIST;
}

unsigned ndobj_ele_list_count_children(const ndobj_ele_list_t *self)
{
    return self->count;
}

bool ndobj_ele_list_add_child(ndobj_ele_list_t *self, ndobj_ele_t *item)
{
    if( !item ) return false;

    item->prev = self->tail;
    item->next = NULL;

    if( self->tail )
        self->tail->next = item;
    else
        self->head = item;

    self->tail = item;

    ++ self->count;

    return true;
}

const ndobj_ele_t* ndobj_ele_list_get_next_child(
    const ndobj_ele_list_t  *self,
    const ndobj_ele_t       *curr,
    const char              *name)
{
    const ndobj_ele_t *item = curr ? curr->next : self->head;

    while( name && item && strcmp(name, ndobj_ele_get_name(item)) )
        item = item->next;

    return item;
}

const ndobj_ele_t* ndobj_ele_list_get_prev_child(
    const ndobj_ele_list_t  *self,
    const ndobj_ele_t       *curr,
    const char              *name)
{
    const ndobj_ele_t *item = curr ? curr->prev : self->tail;

    while( name && item && strcmp(name, ndobj_ele_get_name(item)) )
        item = item->prev;

    return item;
}

size_t ndobj_ele_list_encode(const ndobj_ele_list_t *self, void *buf, size_t bufsize)
{
    // Calculate children size.

    size_t body_size = 0;
    for(const ndobj_ele_t *item = self->head; item; item = item->next)
    {
        size_t size = ndobj_ele_encode(item, NULL, 0);
        if( !size ) return 0;

        body_size += size;
    }

    // Calculate header size.

    size_t head_size =
        ndobj_header_encode(NULL, 0, self->name, NDOBJ_TYPE_LIST, body_size);
    if( !head_size ) return 0;

    size_t total_size = head_size + body_size;
    if( !buf ) return total_size;
    if( bufsize < total_size ) return 0;

    // Encode header.

    uint8_t *pos = buf;

    if( head_size !=
        ndobj_header_encode(pos, head_size, self->name, NDOBJ_TYPE_LIST, body_size) )
    {
        return 0;
    }

    pos += head_size;
    bufsize -= head_size;

    // Encode children.

    for(const ndobj_ele_t *item = self->head; item; item = item->next)
    {
        size_t size = ndobj_ele_encode(item, pos, bufsize);
        if( !size || size > bufsize ) return 0;

        pos += size;
        bufsize -= size;
    }

    return pos - (uint8_t*) buf;
}
