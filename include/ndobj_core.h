/**
 * @file
 * @brief       Core functions.
 * @author      王文佑
 * @date        2019-04-12
 * @copyright   ZLib Licence
 * @see         https://gitlab.com/iceignatius/ndobj
 */
#ifndef _NDOBJ_CORE_H_
#define _NDOBJ_CORE_H_

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

size_t ndobj_length_encode(void *buf, size_t bufsize, size_t value);
size_t ndobj_length_decode(const void *src, size_t srcsize, size_t *value);

size_t ndobj_header_encode(
    void        *buf,
    size_t      bufsize,
    const char  *name,
    int         type,
    size_t      body_size);

size_t ndobj_unit_encode(
    void        *buf,
    size_t      bufsize,
    const char  *name,
    int         type,
    const void  *body,
    size_t      body_size);

size_t ndobj_unit_decode(
    const void  *src,
    size_t      srcsize,
    const char  **name,
    size_t      *name_len,
    int         *type,
    const void  **body,
    size_t      *body_size);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
