#ifndef _NDOBJ_ELEMENT_BINARY_H_
#define _NDOBJ_ELEMENT_BINARY_H_

#include <stdint.h>
#include "ndobj_element.h"
#include "ndobj_element_struct.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct ndobj_ele_binary
{
    ndobj_ele_t super;

    char    *name;
    uint8_t *data;
    size_t  size;

} ndobj_ele_binary_t;

ndobj_ele_binary_t* ndobj_ele_binary_create(
    const char  *name,
    const void  *data,
    size_t      size);
void ndobj_ele_binary_release(ndobj_ele_binary_t *self);

const char* ndobj_ele_binary_get_name(const ndobj_ele_binary_t *self);
int         ndobj_ele_binary_get_type(const ndobj_ele_binary_t *self);

const void* ndobj_ele_binary_get_bindata(const ndobj_ele_binary_t *self);
size_t      ndobj_ele_binary_get_binsize(const ndobj_ele_binary_t *self);

size_t ndobj_ele_binary_encode(const ndobj_ele_binary_t *self, void *buf, size_t bufsize);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
