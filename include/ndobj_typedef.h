/**
 * @file
 * @brief       Definition about NDO types.
 * @author      王文佑
 * @date        2019-04-12
 * @copyright   ZLib Licence
 * @see         https://gitlab.com/iceignatius/ndobj
 */
#ifndef _NDOBJ_TYPEDEF_H_
#define _NDOBJ_TYPEDEF_H_

/**
 * Object types.
 */
enum ndobj_type
{
    NDOBJ_TYPE_BINARY   = 0,    ///< Binary.
    NDOBJ_TYPE_STRING   = 'S',  ///< String.
    NDOBJ_TYPE_INTEGER  = 'I',  ///< Signed integer.
    NDOBJ_TYPE_UNSIGNED = 'U',  ///< Unsigned integer.
    NDOBJ_TYPE_BOOLEAN  = 'B',  ///< Boolean.
    NDOBJ_TYPE_FLOAT    = 'F',  ///< Floating point.
    NDOBJ_TYPE_LIST     = 'L',  ///< List type of container.
    NDOBJ_TYPE_MAP      = 'M',  ///< Map type of container.
};

#endif
