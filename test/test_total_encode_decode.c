#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>
#include "ndobj.h"
#include "test_total_encode_decode.h"

static const uint8_t bindata[] =
{
    0x80 | 'L',
    102,
    'b','i','n','a','r','y',            0x80 | 0,   4, 'd','a','t','a',
    's','t','r','i','n','g',            0x80 | 'S', 4, 't','e','x','t',
    'i','n','t','e','g','e','r',        0x80 | 'I', 4, 0x13, 0x57, 0x24, 0x68,
    'i','n','t','e','g','e','r',        0x80 | 'I', 1, 0xF9,
    'u','n','s','i','g','n','e','d',    0x80 | 'U', 4, 0xAF, 0x57, 0x24, 0x68,
    'b','o','o','l','e','a','n',        0x80 | 'B', 1, 1,
    'f','l','o','a','t',                0x80 | 'F', 8, 0x40, 0x09, 0x21, 0xFB, 0x53, 0xC8, 0xD4, 0xF1,
    'm','a','p',                        0x80 | 'M', 11,
    'c','h','i','l','d',                0x80 | 'S', 4, 't','e','x','t',
};

static
void encode_test(void **state)
{
    ndobj_ele_t *root = ndobj_create_list("");
    assert_non_null(root);

    assert_true(ndobj_ele_add_child(root, ndobj_create_binary("binary", "data", 4)));
    assert_true(ndobj_ele_add_child(root, ndobj_create_string("string", "text")));
    assert_true(ndobj_ele_add_child(root, ndobj_create_integer("integer", 0x13572468)));
    assert_true(ndobj_ele_add_child(root, ndobj_create_integer("integer", -7)));
    assert_true(ndobj_ele_add_child(root, ndobj_create_unsigned("unsigned", 0xAF572468)));
    assert_true(ndobj_ele_add_child(root, ndobj_create_boolean("boolean", true)));
    assert_true(ndobj_ele_add_child(root, ndobj_create_float("float", 3.14159265)));

    ndobj_ele_t *map = ndobj_create_map("map");
    assert_non_null(map);
    assert_true(ndobj_ele_add_child(map, ndobj_create_string("child", "text")));

    assert_true(ndobj_ele_add_child(root, map));

    uint8_t buf[512] = {0};
    assert_int_equal(ndobj_ele_encode(root, NULL, 0), sizeof(bindata));
    assert_int_equal(ndobj_ele_encode(root, buf, sizeof(bindata)-1), 0);
    assert_int_equal(ndobj_ele_encode(root, buf, sizeof(buf)), sizeof(bindata));
    assert_memory_equal(buf, bindata, sizeof(bindata));

    ndobj_ele_release(root);
}

static
void decode_test(void **state)
{
    assert_null(ndobj_parse(bindata, sizeof(bindata)-1));

    ndobj_ele_t *root = ndobj_parse(bindata, sizeof(bindata));
    assert_non_null(root);

    assert_true         (ndobj_ele_is_list(root));
    assert_string_equal (ndobj_ele_get_name(root), "");
    assert_int_equal    (ndobj_ele_count_children(root), 8);

    const ndobj_ele_t *item = NULL;

    assert_non_null(( item = ndobj_ele_get_next_child(root, item, NULL) ));
    assert_string_equal (ndobj_ele_get_name(item), "binary");
    assert_true         (ndobj_ele_is_binary(item));
    assert_int_equal    (ndobj_ele_get_binsize(item), 4);
    assert_memory_equal (ndobj_ele_get_bindata(item), "data", 4);

    assert_non_null(( item = ndobj_ele_get_next_child(root, item, NULL) ));
    assert_string_equal (ndobj_ele_get_name(item), "string");
    assert_true         (ndobj_ele_is_string(item));
    assert_string_equal (ndobj_ele_get_string(item), "text");

    assert_non_null(( item = ndobj_ele_get_next_child(root, item, NULL) ));
    assert_string_equal (ndobj_ele_get_name(item), "integer");
    assert_true         (ndobj_ele_is_integer(item));
    assert_int_equal    (ndobj_ele_get_integer(item), 0x13572468);

    assert_non_null(( item = ndobj_ele_get_next_child(root, item, NULL) ));
    assert_string_equal (ndobj_ele_get_name(item), "integer");
    assert_true         (ndobj_ele_is_integer(item));
    assert_int_equal    (ndobj_ele_get_integer(item), -7);

    assert_non_null(( item = ndobj_ele_get_next_child(root, item, NULL) ));
    assert_string_equal (ndobj_ele_get_name(item), "unsigned");
    assert_true         (ndobj_ele_is_unsigned(item));
    assert_true         (ndobj_ele_get_unsigned(item) == 0xAF572468);

    assert_non_null(( item = ndobj_ele_get_next_child(root, item, NULL) ));
    assert_string_equal (ndobj_ele_get_name(item), "boolean");
    assert_true         (ndobj_ele_is_boolean(item));
    assert_true         (ndobj_ele_get_boolean(item));

    assert_non_null(( item = ndobj_ele_get_next_child(root, item, NULL) ));
    assert_string_equal (ndobj_ele_get_name(item), "float");
    assert_true         (ndobj_ele_is_float(item));
    assert_true         ( abs(ndobj_ele_get_float(item) - 3.14159265) < 0.0001 );

    assert_non_null(( item = ndobj_ele_get_next_child(root, item, NULL) ));
    assert_string_equal (ndobj_ele_get_name(item), "map");
    assert_true         (ndobj_ele_is_map(item));
    assert_int_equal    (ndobj_ele_count_children(item), 1);

    const ndobj_ele_t *child = ndobj_ele_get_first_child(item, NULL);
    assert_non_null(child);
    assert_string_equal (ndobj_ele_get_name(child), "child");
    assert_true         (ndobj_ele_is_string(child));
    assert_string_equal (ndobj_ele_get_string(child), "text");

    assert_null(( item = ndobj_ele_get_next_child(root, item, NULL) ));

    ndobj_ele_release(root);
}

int test_total_encode_decode(void)
{
    struct CMUnitTest tests[] =
    {
        cmocka_unit_test(encode_test),
        cmocka_unit_test(decode_test),
    };

    return cmocka_run_group_tests_name("Encode and Decode Test", tests, NULL, NULL);
}
