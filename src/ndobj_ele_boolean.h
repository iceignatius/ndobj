#ifndef _NDOBJ_ELE_BOOLEAN_H_
#define _NDOBJ_ELE_BOOLEAN_H_

#include "ndobj_element.h"
#include "ndobj_element_struct.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct ndobj_ele_boolean
{
    ndobj_ele_t super;

    char *name;
    bool value;

} ndobj_ele_boolean_t;

ndobj_ele_boolean_t* ndobj_ele_boolean_create(const char *name, bool value);
ndobj_ele_boolean_t* ndobj_ele_boolean_create_import(
    const char  *name,
    const void  *bodydata,
    size_t      bodysize);
void ndobj_ele_boolean_release(ndobj_ele_boolean_t *self);

const char* ndobj_ele_boolean_get_name(const ndobj_ele_boolean_t *self);
int         ndobj_ele_boolean_get_type(const ndobj_ele_boolean_t *self);

bool ndobj_ele_boolean_get_boolean(const ndobj_ele_boolean_t *self);

size_t ndobj_ele_boolean_encode(
    const ndobj_ele_boolean_t *self,
    void    *buf,
    size_t  bufsize);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
