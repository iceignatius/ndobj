#include "ndobj_ele_binary.h"
#include "ndobj_ele_string.h"
#include "ndobj_ele_integer.h"
#include "ndobj_ele_unsigned.h"
#include "ndobj_ele_boolean.h"
#include "ndobj_ele_float.h"
#include "ndobj_ele_list.h"
#include "ndobj_ele_map.h"
#include "ndobj_creator.h"

ndobj_ele_t* ndobj_create_binary(const char *name, const void *data, size_t size)
{
    /**
     * @brief Create and build a binary type of object.
     *
     * @param name Name of the new object.
     * @param data The payload data.
     * @param size The payload size.
     * @return The new created object instance if success; or NULL if failed.
     */
    return (ndobj_ele_t*) ndobj_ele_binary_create(name, data, size);
}

ndobj_ele_t* ndobj_create_string(const char *name, const char *value)
{
    /**
     * @brief Create and build a string type of object.
     *
     * @param name  Name of the new object.
     * @param value The object value.
     * @return The new created object instance if success; or NULL if failed.
     */
    return (ndobj_ele_t*) ndobj_ele_string_create(name, value, -1);
}

ndobj_ele_t* ndobj_create_integer(const char *name, int value)
{
    /**
     * @brief Create and build a signed integer type of object.
     *
     * @param name  Name of the new object.
     * @param value The object value.
     * @return The new created object instance if success; or NULL if failed.
     */
    return (ndobj_ele_t*) ndobj_ele_integer_create(name, value);
}

ndobj_ele_t* ndobj_create_unsigned(const char *name, unsigned value)
{
    /**
     * @brief Create and build a unsigned integer type of object.
     *
     * @param name  Name of the new object.
     * @param value The object value.
     * @return The new created object instance if success; or NULL if failed.
     */
    return (ndobj_ele_t*) ndobj_ele_unsigned_create(name, value);
}

ndobj_ele_t* ndobj_create_boolean(const char *name, bool value)
{
    /**
     * @brief Create and build a boolean type of object.
     *
     * @param name  Name of the new object.
     * @param value The object value.
     * @return The new created object instance if success; or NULL if failed.
     */
    return (ndobj_ele_t*) ndobj_ele_boolean_create(name, value);
}

ndobj_ele_t* ndobj_create_float(const char *name, double value)
{
    /**
     * @brief Create and build a floating point type of object.
     *
     * @param name  Name of the new object.
     * @param value The object value.
     * @return The new created object instance if success; or NULL if failed.
     */
    return (ndobj_ele_t*) ndobj_ele_float_create(name, value);
}

ndobj_ele_t* ndobj_create_list(const char *name)
{
    /**
     * @brief Create and build a list type of object.
     *
     * @param name Name of the new object.
     * @return The new created object instance if success; or NULL if failed.
     */
    return (ndobj_ele_t*) ndobj_ele_list_create(name);
}

ndobj_ele_t* ndobj_create_map(const char *name)
{
    /**
     * @brief Create and build a map type of object.
     *
     * @param name Name of the new object.
     * @return The new created object instance if success; or NULL if failed.
     */
    return (ndobj_ele_t*) ndobj_ele_map_create(name);
}
