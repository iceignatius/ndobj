#include <stdint.h>
#include <string.h>
#include "ndobj_core.h"
#include "ndobj_typedef.h"
#include "ndobj_ele_binary.h"
#include "ndobj_ele_string.h"
#include "ndobj_ele_integer.h"
#include "ndobj_ele_unsigned.h"
#include "ndobj_ele_boolean.h"
#include "ndobj_ele_float.h"
#include "ndobj_ele_list.h"
#include "ndobj_ele_map.h"
#include "ndobj_parser.h"

static
ndobj_ele_t* create_import_object(
    const char  *name_data,
    size_t      name_len,
    int         type,
    const void  *body_data,
    size_t      body_size)
{
    char name[ name_len + 1 ];
    memcpy(name, name_data, name_len);
    name[name_len] = 0;

    switch( type )
    {
    case NDOBJ_TYPE_STRING:
        return (ndobj_ele_t*) ndobj_ele_string_create(name, body_data, body_size);

    case NDOBJ_TYPE_INTEGER:
        return (ndobj_ele_t*) ndobj_ele_integer_create_import(name, body_data, body_size);

    case NDOBJ_TYPE_UNSIGNED:
        return (ndobj_ele_t*) ndobj_ele_unsigned_create_import(name, body_data, body_size);

    case NDOBJ_TYPE_BOOLEAN:
        return (ndobj_ele_t*) ndobj_ele_boolean_create_import(name, body_data, body_size);

    case NDOBJ_TYPE_FLOAT:
        return (ndobj_ele_t*) ndobj_ele_float_create_import(name, body_data, body_size);

    case NDOBJ_TYPE_LIST:
        return (ndobj_ele_t*) ndobj_ele_list_create(name);

    case NDOBJ_TYPE_MAP:
        return (ndobj_ele_t*) ndobj_ele_map_create(name);

    case NDOBJ_TYPE_BINARY:
    default:
        return (ndobj_ele_t*) ndobj_ele_binary_create(name, body_data, body_size);
    }
}

static
bool parse_add_children(ndobj_ele_t *parent, const uint8_t *src, size_t srcsize)
{
    while( srcsize )
    {
        const char  *name;
        size_t      name_len;
        int         type;
        const void  *body;
        size_t      body_size;

        size_t parse_size =
            ndobj_unit_decode(src, srcsize, &name, &name_len, &type, &body, &body_size);
        if( !parse_size || parse_size > srcsize ) return false;

        src += parse_size;
        srcsize -= parse_size;

        ndobj_ele_t *item =
            create_import_object(name, name_len, type, body, body_size);
        if( !item ) return false;

        if( !ndobj_ele_add_child(parent, item) )
        {
            ndobj_ele_release(item);
            return false;
        }

        if( ndobj_ele_is_container(item) &&
            !parse_add_children(item, body, body_size) )
        {
            return false;
        }
    }

    return true;
}

ndobj_ele_t* ndobj_parse(const void *data, size_t size)
{
    /**
     * @brief Parse and create (recursively) NDO objects from binary data.
     *
     * @param data The input data.
     * @param size Size of the input data.
     * @return  The corresponding object instance if success; or
     *          NULL if failed.
     */
    ndobj_ele_t *root = NULL;

    bool succ = false;
    do
    {
        const char  *name;
        size_t      name_len;
        int         type;
        const void  *body;
        size_t      body_size;

        size_t parse_size =
            ndobj_unit_decode(data, size, &name, &name_len, &type, &body, &body_size);
        if( !parse_size || parse_size > size ) break;

        root = create_import_object(name, name_len, type, body, body_size);
        if( !root ) break;

        if( ndobj_ele_is_container(root) &&
            !parse_add_children(root, body, body_size) )
        {
            break;
        }

        succ = true;
    } while(false);

    if( !succ && root )
    {
        ndobj_ele_release(root);
        root = NULL;
    }

    return root;
}
