# Data Format of Named Data Object

Named Data Object (NDO) is a TLV like data format
which be designed to encapsulate data for data exchange purpose.

## NDO Data Group

The NDO data is consists of a sequence of NDO Elemens without any gap or spaces,
we called it is a group of elements.
see the following description as the schematic diagram:

    +-----------------------------------------+
    | NDO element group                       |
    +-----------+-----------+-----+-----------+
    | Element 1 | Element 2 | ... | Element N |
    +-----------+-----------+-----+-----------+

## NDO Data Element

A NDO data element is consists of four parts:
name, name-terminator, length, and data.

    +------------------------------------------------+
    | NDO element                                    |
    +------+-----------------+--------+--------------+
    | Name | Name-terminator | Length | Payload data |
    +------+-----------------+--------+--------------+

### Name

The name is used to identify what the element is meaning for.
The name is a sequence of octets with variable length,
and be followed by a name-terminator.
Each octets must be ASCII printable characters.

### Name-terminator

The name-terminator is one octet data which have two effects:
* Mark the end of element name.
* Indicate the type of payload data.

The most significant bit must be 1,
and the rest 7 bits is the data type value which is an ASCII character.

The data type may be one of the following value:

* 'S': The payload data is a string in UTF-8 format.

* 'I': The payload data is a signed integer in big-endian.
    In this case, the size of payload data must be 1, 2, 4, or 8,
    the other values indicates incorrect data!

* 'U': The payload data is an unsigned integer in big-endian.
    In this case, the size of payload data must be 1, 2, 4, or 8,
    the other values indicates incorrect data!

* 'B': The payload data is an boolean value.
    In this case, the size of payload data must be 1,
    the other values indicates incorrect data!
    The payload value ZERO indicates FALSE;
    and others indicates TRUE.

* 'F': The payload data is a floating point value
    with IEEE-754 format in big-endian.
    In this case, the size of payload data must be 4 or 8,
    the other values indicates incorrect data!

* 'M': This element is a container of more elements,
    it means that the payload data is another group of elements.

    The 'M' means this is the unordered map type of container,
    it means that the order in sequence of sub elements means nothing,
    and the sub elements should not have duplicated name.

    If there are duplicated names in the sub elements,
    then the behaviour of parse is undefined!

* 'L': This element is a container of more elements,
    it means that the payload data is another group of elements.

    The 'L' means this is the ordered list type of container,
    it means that the order in sequence of sub elements is important,
    and there may be duplicated names in the sub elements.

* ZERO: The payload is variable length of binary data, and the formats are free.

* OTHERS: For any unknown or undefined data type value,
    the payload data should be treat as
    the free format and variable length binary data.

### Length

The length field have variable size of data which encodes
the value of size of payload data in octets.

The length field can have one of the encode formats:
"single octet format" and "multiple octets format",
they are designed for optimisation of value range and encoded size.
We will describe them in details below.

#### Single octet format

    +-------------------+
    | Z value (1 octet) |
    +-------------------+

* The size of length field is 1.
* The most significant bit must be 0.
* The octet value is the size of payload data in octets.

#### Multiple octets format

    +------------------+---------------------     --+
    | Header (1 octet) | Z value (N octets)   ...   |
    +------------------+---------------------     --+

* The size of length field is N+1.
* The most significant bit of the first octet must be 1.
* The lower 7 bits of the first octet is an integer value (N).
    + The N value indicates there are N more octets of the size field.
    + The N value must be 2, 4, or 8,
        any value else than these values are invalid!
* Data of the length field without the first byte is
    the binary data of an unsigned integer value (Z) in big-endian format.
    The Z value is the size of payload data in octets.

### Payload Data

This is the major payload data of the NDO element.
