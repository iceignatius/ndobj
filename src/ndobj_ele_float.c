#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <endian.h>
#include "ndobj_typedef.h"
#include "ndobj_core.h"
#include "ndobj_ele_float.h"

static const struct ndobj_ele_vtbl vtbl =
{
    .release = (void(*)(ndobj_ele_t*)) ndobj_ele_float_release,

    .get_name = (const char*(*)(const ndobj_ele_t*))    ndobj_ele_float_get_name,
    .get_type = (int(*)(const ndobj_ele_t*))            ndobj_ele_float_get_type,

    .get_float = (double(*)(const ndobj_ele_t*)) ndobj_ele_float_get_float,

    .encode = (size_t(*)(const ndobj_ele_t*,void*,size_t)) ndobj_ele_float_encode,
};

ndobj_ele_float_t* ndobj_ele_float_create(const char *name, double value)
{
    ndobj_ele_float_t *inst = NULL;

    bool succ = false;
    do
    {
        inst = malloc(sizeof(*inst));
        if( !inst ) break;

        ndobj_ele_init(&inst->super, &vtbl);

        inst->name  = strdup(name ? name : "");
        inst->value = value;
        if( !inst->name ) break;

        succ = true;
    } while(false);

    if( !succ && inst )
    {
        ndobj_ele_float_release(inst);
        inst = NULL;
    }

    return inst;
}

ndobj_ele_float_t* ndobj_ele_float_create_import(
    const char  *name,
    const void  *bodydata,
    size_t      bodysize)
{
    if( !bodydata || !bodysize ) return NULL;

    double value;
    if( bodysize != ndobj_ele_float_decode_body(bodydata, bodysize, &value) )
        return NULL;

    return ndobj_ele_float_create(name, value);
}

void ndobj_ele_float_release(ndobj_ele_float_t *self)
{
    if( self->name )
        free(self->name);

    free(self);
}

const char* ndobj_ele_float_get_name(const ndobj_ele_float_t *self)
{
    return self->name;
}

int ndobj_ele_float_get_type(const ndobj_ele_float_t *self)
{
    return NDOBJ_TYPE_FLOAT;
}

double ndobj_ele_float_get_float(const ndobj_ele_float_t *self)
{
    return self->value;
}

size_t ndobj_ele_float_encode(const ndobj_ele_float_t *self, void *buf, size_t bufsize)
{
    uint8_t bodydata[8];
    size_t bodysize =
        ndobj_ele_float_encode_body(
            buf ? bodydata : NULL,
            sizeof(bodydata),
            self->value);

    if( !bodysize )
        return 0;

    return
        ndobj_unit_encode(
            buf,
            bufsize,
            self->name,
            NDOBJ_TYPE_FLOAT,
            bodydata,
            bodysize);
}

static
void write_float64be(uint8_t *buf, double value)
{
    union
    {
        double      fval;
        uint64_t    uval;
        uint8_t     bytes[8];
    } exg;
    assert( sizeof(exg.fval) == sizeof(exg.uval) );

    exg.fval = value;
    exg.uval = htobe64(exg.uval);
    memcpy(buf, exg.bytes, sizeof(exg.bytes));
}

static
double read_float64be(const uint8_t *src)
{
    union
    {
        double      fval;
        uint64_t    uval;
        uint8_t     bytes[8];
    } exg;
    assert( sizeof(exg.fval) == sizeof(exg.uval) );

    memcpy(exg.bytes, src, sizeof(exg.bytes));
    exg.uval = be64toh(exg.uval);
    return exg.fval;
}

static
double read_float32be(const uint8_t *src)
{
    union
    {
        float       fval;
        uint32_t    uval;
        uint8_t     bytes[4];
    } exg;
    assert( sizeof(exg.fval) == sizeof(exg.uval) );

    memcpy(exg.bytes, src, sizeof(exg.bytes));
    exg.uval = be32toh(exg.uval);
    return exg.fval;
}

size_t ndobj_ele_float_encode_body(void *buf, size_t bufsize, double value)
{
    if( !buf ) return 8;
    if( bufsize < 8 ) return 0;

    write_float64be(buf, value);
    return 8;
}

size_t ndobj_ele_float_decode_body(const void *src, size_t srcsize, double *value)
{
    if( !src || !value ) return 0;

    switch( srcsize )
    {
    case 8:
        *value = read_float64be(src);
        break;

    case 4:
        *value = read_float32be(src);
        break;

    default:
        return 0;
    }

    return srcsize;
}
