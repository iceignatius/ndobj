# Library of Named Data Object

Named Data Object (NDO) is a TLV like data format
which be designed to encapsulate data for data exchange purpose,
please see the "DataFormat" document for more details about the format it self.

We also support the corresponding library (NDOBJ) of the NDO format
to help people to encode, decode, and search data easily.
