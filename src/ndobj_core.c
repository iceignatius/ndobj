#include <string.h>
#include "ndobj_intdat.h"
#include "ndobj_core.h"

static
size_t calc_length_body_size_by_value(size_t value)
{
    if( value < 0x80 )
        return 1;
    else if( value < 0x8000 )
        return 2;
    else if( value < 0x80000000ULL )
        return 4;
    else
        return 8;
}

size_t ndobj_length_encode(void *buf, size_t bufsize, size_t value)
{
    /**
     * @brief Encode NDO length value to binary data.
     *
     * @param buf       A buffer to receive the encoded data.
     *                  This parameter can be NULL to calculate the encoded size only.
     * @param bufsize   Size of the output buffer.
     * @param value     The length value to be encoded.
     *
     * @retval >0   The number of bytes of data be filled to the output buffer;
     *              or the minimum size required of the output buffer if @a buf is NULL.
     * @retval ZERO Failed to encode data, ie. the output buffer is too small.
     */
    size_t body_size = calc_length_body_size_by_value(value);
    size_t head_size = body_size == 1 ? 0 : 1;

    size_t total_size = head_size + body_size;
    if( !buf ) return total_size;
    if( bufsize < total_size ) return 0;

    uint8_t *pos = buf;

    if( head_size )
        *pos++ = 0x80 | body_size;

    switch( body_size )
    {
    case 1:
        *pos = value;
        break;

    case 2:
        ndobj_intdat_write_uint16be(pos, value);
        break;

    case 4:
        ndobj_intdat_write_uint32be(pos, value);
        break;

    case 8:
        ndobj_intdat_write_uint64be(pos, value);
        break;

    default:
        return 0;
    }

    return total_size;
}

size_t ndobj_length_decode(const void *src, size_t srcsize, size_t *value)
{
    /**
     * @brief Decode NDO length value from binary data.
     *
     * @param[in]   src     The input data.
     * @param[in]   srcsize Size of the input data.
     * @param[out]  value   Returns the decoded length value.
     *
     * @retval >0   Total size of the input data be parsed.
     * @retval ZERO Decode failed.
     */
    if( !src || !srcsize ) return 0;

    const uint8_t *pos = src;

    size_t head_size = pos[0] & 0x80 ? 1 : 0;
    size_t body_size = head_size ? pos[0] & 0x7F : 1;
    size_t total_size = head_size + body_size;
    if( srcsize < total_size ) return 0;
    if( !value ) return total_size;

    if( head_size )
        ++pos;

    switch( body_size )
    {
    case 1:
        *value = *pos;
        break;

    case 2:
        *value = ndobj_intdat_read_uint16be(pos);
        break;

    case 4:
        *value = ndobj_intdat_read_uint32be(pos);
        break;

    case 8:
        *value = ndobj_intdat_read_uint64be(pos);
        break;

    default:
        return 0;
    }

    return total_size;
}

size_t ndobj_header_encode(
    void        *buf,
    size_t      bufsize,
    const char  *name,
    int         type,
    size_t      body_size)
{
    /**
     * @brief Encode a binary format NDO object header.
     *
     * @param buf       A buffer to receive the encoded data.
     *                  This parameter can be NULL to calculate the encoded size only.
     * @param bufsize   Size of the output buffer.
     * @param name      The object name, all characters must be ASCII printable.
     * @param type      The object type, see ::ndobj_type for predefined values.
     * @param body_size Size of the payload.
     *
     * @retval >0   The number of bytes of data be filled to the output buffer;
     *              or the minimum size required of the output buffer if @a buf is NULL.
     * @retval ZERO Failed to encode data, ie. the output buffer is too small.
     */
    size_t len_size = ndobj_length_encode(NULL, 0, body_size);
    size_t name_len = name ? strlen(name) : 0;
    size_t total_size = name_len + 1 + len_size;
    if( !buf ) return total_size;
    if( bufsize < total_size ) return 0;

    uint8_t *pos = buf;

    memcpy(pos, name, name_len);
    pos += name_len;

    *pos++ = 0x80 | ( 0x7F & type );

    pos += ndobj_length_encode(pos, len_size, body_size);

    return total_size;
}

size_t ndobj_unit_encode(
    void        *buf,
    size_t      bufsize,
    const char  *name,
    int         type,
    const void  *body,
    size_t      body_size)
{
    /**
     * @brief Encode a binary format NDO object data.
     *
     * @param buf       A buffer to receive the encoded data.
     *                  This parameter can be NULL to calculate the encoded size only.
     * @param bufsize   Size of the output buffer.
     * @param name      The object name, all characters must be ASCII printable.
     * @param type      The object type, see ::ndobj_type for predefined values.
     * @param body      The payload data.
     *                  This parameter can be NULL to not carry payload data.
     * @param body_size Size of the payload.
     *
     * @retval >0   The number of bytes of data be filled to the output buffer;
     *              or the minimum size required of the output buffer if @a buf is NULL.
     * @retval ZERO Failed to encode data, ie. the output buffer is too small.
     */
    body_size = body ? body_size : 0;
    size_t head_size = ndobj_header_encode(NULL, 0, name, type, body_size);
    if( !head_size ) return 0;

    size_t total_size = head_size + body_size;
    if( !buf ) return total_size;
    if( bufsize < total_size ) return 0;

    uint8_t *pos = buf;

    if( head_size != ndobj_header_encode(pos, head_size, name, type, body_size) )
        return 0;

    memcpy(pos + head_size, body, body_size);

    return total_size;
}

static
size_t calc_name_length(const uint8_t *data)
{
    size_t len = 0;
    while( !( *data++ & 0x80 ) )
        ++len;

    return len;
}

size_t ndobj_unit_decode(
    const void  *src,
    size_t      srcsize,
    const char  **name,
    size_t      *name_len,
    int         *type,
    const void  **body,
    size_t      *body_size)
{
    /**
     * @brief Decode information of a single NDO object from binary data.
     *
     * @param[in]   src         The input data.
     * @param[in]   srcsize     Size of the input data.
     * @param[out]  name        Returns the object name (not null-terminated) when success.
     * @param[out]  name_len    Returns the object name length when success.
     * @param[out]  type        Returns the object type when success.
     * @param[out]  body        Returns the object payload data when success.
     * @param[out]  body_size   Returns the object payload data size when success.
     *
     * @retval >0   Total size of the input data be parsed.
     * @retval ZERO Decode failed.
     */
    if( !src || !srcsize ) return 0;

    const uint8_t *pos = src;

    if( name )
        *name = (const char*) pos;

    // Calculate name length.

    size_t field_len = calc_name_length(pos);

    if( name_len )
        *name_len = field_len;
    pos += field_len;

    // Decode type field.

    if( type )
        *type = *pos & 0x7F;
    ++pos;

    // Decode length field.

    size_t payload_size;
    size_t restsize = srcsize - ( pos - (const uint8_t*) src );
    field_len = ndobj_length_decode(pos, restsize, &payload_size);
    if( !field_len ) return 0;

    if( body_size )
        *body_size = payload_size;
    pos += field_len;

    // Decode payload field.

    if( body )
        *body = pos;
    pos += payload_size;

    return pos - (const uint8_t*) src;
}
