/**
 * @file
 * @brief       Object creator.
 * @author      王文佑
 * @date        2019-04-12
 * @copyright   ZLib Licence
 * @see         https://gitlab.com/iceignatius/ndobj
 */
#ifndef _NDOBJ_CREATOR_H_
#define _NDOBJ_CREATOR_H_

#include "ndobj_element.h"

#ifdef __cplusplus
extern "C" {
#endif

ndobj_ele_t* ndobj_create_binary(const char *name, const void *data, size_t size);
ndobj_ele_t* ndobj_create_string(const char *name, const char *value);
ndobj_ele_t* ndobj_create_integer(const char *name, int value);
ndobj_ele_t* ndobj_create_unsigned(const char *name, unsigned value);
ndobj_ele_t* ndobj_create_boolean(const char *name, bool value);
ndobj_ele_t* ndobj_create_float(const char *name, double value);
ndobj_ele_t* ndobj_create_list(const char *name);
ndobj_ele_t* ndobj_create_map(const char *name);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
