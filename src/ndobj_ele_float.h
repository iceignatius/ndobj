#ifndef _NDOBJ_ELE_FLOAT_H_
#define _NDOBJ_ELE_FLOAT_H_

#include "ndobj_element.h"
#include "ndobj_element_struct.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct ndobj_ele_float
{
    ndobj_ele_t super;

    char    *name;
    double  value;

} ndobj_ele_float_t;

ndobj_ele_float_t* ndobj_ele_float_create(const char *name, double value);
ndobj_ele_float_t* ndobj_ele_float_create_import(
    const char  *name,
    const void  *bodydata,
    size_t      bodysize);
void ndobj_ele_float_release(ndobj_ele_float_t *self);

const char* ndobj_ele_float_get_name(const ndobj_ele_float_t *self);
int         ndobj_ele_float_get_type(const ndobj_ele_float_t *self);

double ndobj_ele_float_get_float(const ndobj_ele_float_t *self);

size_t ndobj_ele_float_encode(const ndobj_ele_float_t *self, void *buf, size_t bufsize);

size_t ndobj_ele_float_encode_body(void *buf, size_t bufsize, double value);
size_t ndobj_ele_float_decode_body(const void *src, size_t srcsize, double *value);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
