#
# NDOBJ Project
#
cmake_minimum_required(VERSION 3.5)
project(ndobj)

add_subdirectory(lib)

find_library(HAVE_CMOCKA cmocka)
message("cmocka: ${HAVE_CMOCKA}")
if(HAVE_CMOCKA)
    enable_testing()
    add_subdirectory(test)
    add_test(unit_tests ${CMAKE_BINARY_DIR}/test/ndobj_test)
endif()
