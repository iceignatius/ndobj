#ifndef _NDOBJ_ELEMENT_STRUCT_H_
#define _NDOBJ_ELEMENT_STRUCT_H_

#include <stddef.h>
#include <stdbool.h>
#include <stdatomic.h>

struct ndobj_ele;

struct ndobj_ele_vtbl
{
    void(*release)(struct ndobj_ele*);

    const char* (*get_name)(const struct ndobj_ele*);
    int         (*get_type)(const struct ndobj_ele*);

    const void* (*get_bindata)(const struct ndobj_ele*);
    size_t      (*get_binsize)(const struct ndobj_ele*);
    const char* (*get_string)(const struct ndobj_ele*);
    int         (*get_integer)(const struct ndobj_ele*);
    unsigned    (*get_unsigned)(const struct ndobj_ele*);
    bool        (*get_boolean)(const struct ndobj_ele*);
    double      (*get_float)(const struct ndobj_ele*);

    unsigned    (*count_children)(const struct ndobj_ele*);
    bool        (*add_child)(struct ndobj_ele*, struct ndobj_ele*);

    const struct ndobj_ele*(*get_next_child)(
        const struct ndobj_ele*,
        const struct ndobj_ele*,
        const char*);
    const struct ndobj_ele*(*get_prev_child)(
        const struct ndobj_ele*,
        const struct ndobj_ele*,
        const char*);

    size_t(*encode)(const struct ndobj_ele*, void*, size_t);
};

struct ndobj_ele
{
    const struct ndobj_ele_vtbl *vtbl;

    atomic_int refcnt;

    struct ndobj_ele *prev;
    struct ndobj_ele *next;
};

static inline
void ndobj_ele_init(struct ndobj_ele *ele, const struct ndobj_ele_vtbl *vtbl)
{
    ele->vtbl = vtbl;

    atomic_init(&ele->refcnt, 1);

    ele->prev = NULL;
    ele->next = NULL;
}

#endif
