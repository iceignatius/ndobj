#ifndef _NDOBJ_ELE_UNSIGNED_H_
#define _NDOBJ_ELE_UNSIGNED_H_

#include "ndobj_element.h"
#include "ndobj_element_struct.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct ndobj_ele_unsigned
{
    ndobj_ele_t super;

    char        *name;
    unsigned    value;

} ndobj_ele_unsigned_t;

ndobj_ele_unsigned_t* ndobj_ele_unsigned_create(const char *name, unsigned value);
ndobj_ele_unsigned_t* ndobj_ele_unsigned_create_import(
    const char  *name,
    const void  *bodydata,
    size_t      bodysize);
void ndobj_ele_unsigned_release(ndobj_ele_unsigned_t *self);

const char* ndobj_ele_unsigned_get_name(const ndobj_ele_unsigned_t *self);
int         ndobj_ele_unsigned_get_type(const ndobj_ele_unsigned_t *self);

unsigned ndobj_ele_unsigned_get_unsigned(const ndobj_ele_unsigned_t *self);

size_t ndobj_ele_unsigned_encode(
    const ndobj_ele_unsigned_t *self,
    void    *buf,
    size_t  bufsize);

size_t ndobj_ele_unsigned_encode_body(void *buf, size_t bufsize, unsigned value);
size_t ndobj_ele_unsigned_decode_body(const void *src, size_t srcsize, unsigned *value);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
