#ifndef _NDOBJ_ELE_STRING_H_
#define _NDOBJ_ELE_STRING_H_

#include "ndobj_element.h"
#include "ndobj_element_struct.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct ndobj_ele_string
{
    ndobj_ele_t super;

    char    *name;
    char    *value;
    size_t  len;

} ndobj_ele_string_t;

ndobj_ele_string_t* ndobj_ele_string_create(
    const char  *name,
    const char  *value,
    size_t      maxlen);
void ndobj_ele_string_release(ndobj_ele_string_t *self);

const char* ndobj_ele_string_get_name(const ndobj_ele_string_t *self);
int         ndobj_ele_string_get_type(const ndobj_ele_string_t *self);

const char* ndobj_ele_string_get_string(const ndobj_ele_string_t *self);

size_t ndobj_ele_string_encode(const ndobj_ele_string_t *self, void *buf, size_t bufsize);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
