#include "test_core.h"
#include "test_element.h"
#include "test_total_encode_decode.h"

int main(void)
{
    int ret;

    if(( ret = test_core() )) return ret;
    if(( ret = test_element() )) return ret;
    if(( ret = test_total_encode_decode() )) return ret;

    return 0;
}
