#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>
#include "ndobj.h"
#include "test_element.h"

static
void binary_element_test(void **state)
{
    static const char name[] = "tag";
    static const uint8_t body[] = { 'B', 'o', 'd', 'y', 'D', 'a', 't', 'a' };

    static const uint8_t bindata[] =
    {
        't', 'a', 'g',
        0x80 | 0,
        8,
        'B', 'o', 'd', 'y', 'D', 'a', 't', 'a',
    };

    ndobj_ele_t *obj = ndobj_create_binary(name, body, sizeof(body));
    assert_non_null(obj);

    assert_string_equal(ndobj_ele_get_name(obj), name);
    assert_int_equal(ndobj_ele_get_type(obj), NDOBJ_TYPE_BINARY);

    assert_int_equal(ndobj_ele_get_binsize(obj), sizeof(body));
    assert_memory_equal(ndobj_ele_get_bindata(obj), body, sizeof(body));

    uint8_t buf[512] = {0};
    assert_int_equal(ndobj_ele_encode(obj, NULL, 0), sizeof(bindata));
    assert_int_equal(ndobj_ele_encode(obj, buf, sizeof(bindata)-1), 0);
    assert_int_equal(ndobj_ele_encode(obj, buf, sizeof(buf)), sizeof(bindata));
    assert_memory_equal(buf, bindata, sizeof(bindata));

    ndobj_ele_release(obj);
}

static
void string_element_test(void **state)
{
    static const char name[] = "tag";
    static const char value[] = "dummy-string";

    static const uint8_t bindata[] =
    {
        't', 'a', 'g',
        0x80 | 'S',
        12,
        'd', 'u', 'm', 'm', 'y', '-', 's', 't', 'r', 'i', 'n', 'g',
    };

    ndobj_ele_t *obj = ndobj_create_string(name, value);
    assert_non_null(obj);

    assert_string_equal(ndobj_ele_get_name(obj), name);
    assert_int_equal(ndobj_ele_get_type(obj), NDOBJ_TYPE_STRING);

    assert_string_equal(ndobj_ele_get_string(obj), value);

    uint8_t buf[512] = {0};
    assert_int_equal(ndobj_ele_encode(obj, NULL, 0), sizeof(bindata));
    assert_int_equal(ndobj_ele_encode(obj, buf, sizeof(bindata)-1), 0);
    assert_int_equal(ndobj_ele_encode(obj, buf, sizeof(buf)), sizeof(bindata));
    assert_memory_equal(buf, bindata, sizeof(bindata));

    ndobj_ele_release(obj);
}

static
void integer_element_test(void **state)
{
    {
        static const char name[] = "tag";
        static const int value = 0x13572468;

        static const uint8_t bindata[] =
        {
            't', 'a', 'g',
            0x80 | 'I',
            4,
            0x13, 0x57, 0x24, 0x68
        };

        ndobj_ele_t *obj = ndobj_create_integer(name, value);
        assert_non_null(obj);

        assert_string_equal(ndobj_ele_get_name(obj), name);
        assert_int_equal(ndobj_ele_get_type(obj), NDOBJ_TYPE_INTEGER);

        assert_int_equal(ndobj_ele_get_integer(obj), value);

        uint8_t buf[512] = {0};
        assert_int_equal(ndobj_ele_encode(obj, NULL, 0), sizeof(bindata));
        assert_int_equal(ndobj_ele_encode(obj, buf, sizeof(bindata)-1), 0);
        assert_int_equal(ndobj_ele_encode(obj, buf, sizeof(buf)), sizeof(bindata));
        assert_memory_equal(buf, bindata, sizeof(bindata));

        ndobj_ele_release(obj);
    }

    {
        static const char name[] = "tag";
        static const int value = -7;

        static const uint8_t bindata[] =
        {
            't', 'a', 'g',
            0x80 | 'I',
            1,
            0xF9
        };

        ndobj_ele_t *obj = ndobj_create_integer(name, value);
        assert_non_null(obj);

        assert_string_equal(ndobj_ele_get_name(obj), name);
        assert_int_equal(ndobj_ele_get_type(obj), NDOBJ_TYPE_INTEGER);

        assert_int_equal(ndobj_ele_get_integer(obj), value);

        uint8_t buf[512] = {0};
        assert_int_equal(ndobj_ele_encode(obj, NULL, 0), sizeof(bindata));
        assert_int_equal(ndobj_ele_encode(obj, buf, sizeof(bindata)-1), 0);
        assert_int_equal(ndobj_ele_encode(obj, buf, sizeof(buf)), sizeof(bindata));
        assert_memory_equal(buf, bindata, sizeof(bindata));

        ndobj_ele_release(obj);
    }
}

static
void unsigned_element_test(void **state)
{
    static const char name[] = "tag";
    static const unsigned value = 0xAF572468;

    static const uint8_t bindata[] =
    {
        't', 'a', 'g',
        0x80 | 'U',
        4,
        0xAF, 0x57, 0x24, 0x68
    };

    ndobj_ele_t *obj = ndobj_create_unsigned(name, value);
    assert_non_null(obj);

    assert_string_equal(ndobj_ele_get_name(obj), name);
    assert_int_equal(ndobj_ele_get_type(obj), NDOBJ_TYPE_UNSIGNED);

    assert_true( ndobj_ele_get_unsigned(obj) == value );

    uint8_t buf[512] = {0};
    assert_int_equal(ndobj_ele_encode(obj, NULL, 0), sizeof(bindata));
    assert_int_equal(ndobj_ele_encode(obj, buf, sizeof(bindata)-1), 0);
    assert_int_equal(ndobj_ele_encode(obj, buf, sizeof(buf)), sizeof(bindata));
    assert_memory_equal(buf, bindata, sizeof(bindata));

    ndobj_ele_release(obj);
}

static
void boolean_element_test(void **state)
{
    {
        static const char name[] = "tag";
        static const bool value = false;

        static const uint8_t bindata[] =
        {
            't', 'a', 'g',
            0x80 | 'B',
            1,
            0
        };

        ndobj_ele_t *obj = ndobj_create_boolean(name, value);
        assert_non_null(obj);

        assert_string_equal(ndobj_ele_get_name(obj), name);
        assert_int_equal(ndobj_ele_get_type(obj), NDOBJ_TYPE_BOOLEAN);

        assert_false(ndobj_ele_get_boolean(obj));

        uint8_t buf[512] = {0};
        assert_int_equal(ndobj_ele_encode(obj, NULL, 0), sizeof(bindata));
        assert_int_equal(ndobj_ele_encode(obj, buf, sizeof(bindata)-1), 0);
        assert_int_equal(ndobj_ele_encode(obj, buf, sizeof(buf)), sizeof(bindata));
        assert_memory_equal(buf, bindata, sizeof(bindata));

        ndobj_ele_release(obj);
    }

    {
        static const char name[] = "tag";
        static const bool value = true;

        static const uint8_t bindata[] =
        {
            't', 'a', 'g',
            0x80 | 'B',
            1,
            1
        };

        ndobj_ele_t *obj = ndobj_create_boolean(name, value);
        assert_non_null(obj);

        assert_string_equal(ndobj_ele_get_name(obj), name);
        assert_int_equal(ndobj_ele_get_type(obj), NDOBJ_TYPE_BOOLEAN);

        assert_true(ndobj_ele_get_boolean(obj));

        uint8_t buf[512] = {0};
        assert_int_equal(ndobj_ele_encode(obj, NULL, 0), sizeof(bindata));
        assert_int_equal(ndobj_ele_encode(obj, buf, sizeof(bindata)-1), 0);
        assert_int_equal(ndobj_ele_encode(obj, buf, sizeof(buf)), sizeof(bindata));
        assert_memory_equal(buf, bindata, sizeof(bindata));

        ndobj_ele_release(obj);
    }
}

static
void floating_element_test(void **state)
{
    static const char name[] = "tag";
    static const double value = 3.14159265;

    static const uint8_t bindata[] =
    {
        't', 'a', 'g',
        0x80 | 'F',
        8,
        0x40, 0x09, 0x21, 0xFB, 0x53, 0xC8, 0xD4, 0xF1
    };

    ndobj_ele_t *obj = ndobj_create_float(name, value);
    assert_non_null(obj);

    assert_string_equal(ndobj_ele_get_name(obj), name);
    assert_int_equal(ndobj_ele_get_type(obj), NDOBJ_TYPE_FLOAT);

    assert_true( abs(ndobj_ele_get_float(obj) - value) < 0.0001 );

    uint8_t buf[512] = {0};
    assert_int_equal(ndobj_ele_encode(obj, NULL, 0), sizeof(bindata));
    assert_int_equal(ndobj_ele_encode(obj, buf, sizeof(bindata)-1), 0);
    assert_int_equal(ndobj_ele_encode(obj, buf, sizeof(buf)), sizeof(bindata));
    assert_memory_equal(buf, bindata, sizeof(bindata));

    ndobj_ele_release(obj);
}

static
void list_element_test(void **state)
{
    static const char name[] = "tag";

    static const uint8_t bindata[] =
    {
        't', 'a', 'g',
        0x80 | 'L',
        32,
        'b', 'o', 'y',      0x80 | 'I', 1, 1,
        'g', 'i', 'r', 'l', 0x80 | 'I', 1, 11,
        'b', 'o', 'y',      0x80 | 'I', 1, 2,
        'g', 'i', 'r', 'l', 0x80 | 'I', 1, 12,
        'b', 'o', 'y',      0x80 | 'I', 1, 3,
    };

    // Build test object.

    ndobj_ele_t *obj = ndobj_create_list(name);
    assert_non_null(obj);

    {
        ndobj_ele_t *item;

        assert_non_null(( item = ndobj_create_integer("boy", 1) ));
        assert_true(ndobj_ele_add_child(obj, item));

        assert_non_null(( item = ndobj_create_integer("girl", 11) ));
        assert_true(ndobj_ele_add_child(obj, item));

        assert_non_null(( item = ndobj_create_integer("boy", 2) ));
        assert_true(ndobj_ele_add_child(obj, item));

        assert_non_null(( item = ndobj_create_integer("girl", 12) ));
        assert_true(ndobj_ele_add_child(obj, item));

        assert_non_null(( item = ndobj_create_integer("boy", 3) ));
        assert_true(ndobj_ele_add_child(obj, item));
    }

    // Check properties.
    assert_string_equal(ndobj_ele_get_name(obj), name);
    assert_int_equal(ndobj_ele_get_type(obj), NDOBJ_TYPE_LIST);
    assert_int_equal(ndobj_ele_count_children(obj), 5);

    // Access all children from head.
    {
        const ndobj_ele_t *item = NULL;

        assert_non_null(( item = ndobj_ele_get_next_child(obj, item, NULL) ));
        assert_int_equal(ndobj_ele_get_integer(item), 1);
        assert_ptr_equal(item, ndobj_ele_get_first_child(obj, NULL));

        assert_non_null(( item = ndobj_ele_get_next_child(obj, item, NULL) ));
        assert_int_equal(ndobj_ele_get_integer(item), 11);

        assert_non_null(( item = ndobj_ele_get_next_child(obj, item, NULL) ));
        assert_int_equal(ndobj_ele_get_integer(item), 2);

        assert_non_null(( item = ndobj_ele_get_next_child(obj, item, NULL) ));
        assert_int_equal(ndobj_ele_get_integer(item), 12);

        assert_non_null(( item = ndobj_ele_get_next_child(obj, item, NULL) ));
        assert_int_equal(ndobj_ele_get_integer(item), 3);

        assert_null(( item = ndobj_ele_get_next_child(obj, item, NULL) ));
    }

    // Access all children from tail.
    {
        const ndobj_ele_t *item = NULL;

        assert_non_null(( item = ndobj_ele_get_prev_child(obj, item, NULL) ));
        assert_int_equal(ndobj_ele_get_integer(item), 3);
        assert_ptr_equal(item, ndobj_ele_get_last_child(obj, NULL));

        assert_non_null(( item = ndobj_ele_get_prev_child(obj, item, NULL) ));
        assert_int_equal(ndobj_ele_get_integer(item), 12);

        assert_non_null(( item = ndobj_ele_get_prev_child(obj, item, NULL) ));
        assert_int_equal(ndobj_ele_get_integer(item), 2);

        assert_non_null(( item = ndobj_ele_get_prev_child(obj, item, NULL) ));
        assert_int_equal(ndobj_ele_get_integer(item), 11);

        assert_non_null(( item = ndobj_ele_get_prev_child(obj, item, NULL) ));
        assert_int_equal(ndobj_ele_get_integer(item), 1);

        assert_null(( item = ndobj_ele_get_prev_child(obj, item, NULL) ));
    }

    // Access children by name from head.
    {
        const ndobj_ele_t *item = NULL;

        assert_non_null(( item = ndobj_ele_get_next_child(obj, item, "boy") ));
        assert_int_equal(ndobj_ele_get_integer(item), 1);
        assert_ptr_equal(item, ndobj_ele_get_first_child(obj, "boy"));

        assert_non_null(( item = ndobj_ele_get_next_child(obj, item, "boy") ));
        assert_int_equal(ndobj_ele_get_integer(item), 2);

        assert_non_null(( item = ndobj_ele_get_next_child(obj, item, "boy") ));
        assert_int_equal(ndobj_ele_get_integer(item), 3);

        assert_null(( item = ndobj_ele_get_next_child(obj, item, "boy") ));
    }

    // Access children by name from tail.
    {
        const ndobj_ele_t *item = NULL;

        assert_non_null(( item = ndobj_ele_get_prev_child(obj, item, "girl") ));
        assert_int_equal(ndobj_ele_get_integer(item), 12);
        assert_ptr_equal(item, ndobj_ele_get_last_child(obj, "girl"));

        assert_non_null(( item = ndobj_ele_get_prev_child(obj, item, "girl") ));
        assert_int_equal(ndobj_ele_get_integer(item), 11);

        assert_null(( item = ndobj_ele_get_prev_child(obj, item, "girl") ));
    }

    // Encode test.
    uint8_t buf[512] = {0};
    assert_int_equal(ndobj_ele_encode(obj, NULL, 0), sizeof(bindata));
    assert_int_equal(ndobj_ele_encode(obj, buf, sizeof(bindata)-1), 0);
    assert_int_equal(ndobj_ele_encode(obj, buf, sizeof(buf)), sizeof(bindata));
    assert_memory_equal(buf, bindata, sizeof(bindata));

    // Release.
    ndobj_ele_release(obj);
}

static
void map_element_test(void **state)
{
    static const char name[] = "tag";

    static const uint8_t bindata[] =
    {
        't', 'a', 'g',
        0x80 | 'M',
        6,
        'g', 'i', 'r', 'l', 0x80 | 0, 0,
    };

    // Build test object.

    ndobj_ele_t *obj = ndobj_create_map(name);
    assert_non_null(obj);

    {
        ndobj_ele_t *item;

        // Duplicated item.
        assert_non_null(( item = ndobj_create_binary("girl", NULL, 0) ));
        assert_true(ndobj_ele_add_child(obj, item));

        assert_non_null(( item = ndobj_create_binary("girl", NULL, 0) ));
        assert_true(ndobj_ele_add_child(obj, item));
    }

    // Check properties.
    assert_string_equal(ndobj_ele_get_name(obj), name);
    assert_int_equal(ndobj_ele_get_type(obj), NDOBJ_TYPE_MAP);
    assert_int_equal(ndobj_ele_count_children(obj), 1);

    // Access children by name from head.
    {
        const ndobj_ele_t *item = NULL;

        assert_non_null(( item = ndobj_ele_get_first_child(obj, "girl") ));
        assert_null(( item = ndobj_ele_get_first_child(obj, "boy") ));
    }

    // Access children by name from tail.
    {
        const ndobj_ele_t *item = NULL;

        assert_non_null(( item = ndobj_ele_get_last_child(obj, "girl") ));
        assert_null(( item = ndobj_ele_get_last_child(obj, "boy") ));
    }

    // Encode test.
    uint8_t buf[512] = {0};
    assert_int_equal(ndobj_ele_encode(obj, NULL, 0), sizeof(bindata));
    assert_int_equal(ndobj_ele_encode(obj, buf, sizeof(bindata)-1), 0);
    assert_int_equal(ndobj_ele_encode(obj, buf, sizeof(buf)), sizeof(bindata));
    assert_memory_equal(buf, bindata, sizeof(bindata));

    // Release.
    ndobj_ele_release(obj);
}

int test_element(void)
{
    struct CMUnitTest tests[] =
    {
        cmocka_unit_test(binary_element_test),
        cmocka_unit_test(string_element_test),
        cmocka_unit_test(integer_element_test),
        cmocka_unit_test(unsigned_element_test),
        cmocka_unit_test(boolean_element_test),
        cmocka_unit_test(floating_element_test),
        cmocka_unit_test(list_element_test),
        cmocka_unit_test(map_element_test),
    };

    return cmocka_run_group_tests_name("Element Object Test", tests, NULL, NULL);
}
