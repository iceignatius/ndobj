/**
 * @file
 * @brief       NDO element.
 * @author      王文佑
 * @date        2019-04-12
 * @copyright   ZLib Licence
 * @see         https://gitlab.com/iceignatius/ndobj
 */
#ifndef _NDOBJ_ELEMENT_H_
#define _NDOBJ_ELEMENT_H_

#include <stddef.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @class ndobj_ele
 * @brief NDO element.
 */
typedef struct ndobj_ele ndobj_ele_t;

void ndobj_ele_release(ndobj_ele_t *self);

ndobj_ele_t* ndobj_ele_newref(ndobj_ele_t *self);

const char* ndobj_ele_get_name(const ndobj_ele_t *self);
int         ndobj_ele_get_type(const ndobj_ele_t *self);

bool ndobj_ele_is_binary(const ndobj_ele_t *self);
bool ndobj_ele_is_string(const ndobj_ele_t *self);
bool ndobj_ele_is_integer(const ndobj_ele_t *self);
bool ndobj_ele_is_unsigned(const ndobj_ele_t *self);
bool ndobj_ele_is_boolean(const ndobj_ele_t *self);
bool ndobj_ele_is_float(const ndobj_ele_t *self);
bool ndobj_ele_is_list(const ndobj_ele_t *self);
bool ndobj_ele_is_map(const ndobj_ele_t *self);
bool ndobj_ele_is_container(const ndobj_ele_t *self);

const void* ndobj_ele_get_bindata(const ndobj_ele_t *self);
size_t      ndobj_ele_get_binsize(const ndobj_ele_t *self);
const char* ndobj_ele_get_string(const ndobj_ele_t *self);
int         ndobj_ele_get_integer(const ndobj_ele_t *self);
unsigned    ndobj_ele_get_unsigned(const ndobj_ele_t *self);
bool        ndobj_ele_get_boolean(const ndobj_ele_t *self);
double      ndobj_ele_get_float(const ndobj_ele_t *self);

unsigned    ndobj_ele_count_children(const ndobj_ele_t *self);
bool        ndobj_ele_add_child(ndobj_ele_t *self, ndobj_ele_t *item);

const ndobj_ele_t* ndobj_ele_get_first_child(
    const ndobj_ele_t   *self,
    const char          *name);
const ndobj_ele_t* ndobj_ele_get_last_child(
    const ndobj_ele_t   *self,
    const char          *name);
const ndobj_ele_t* ndobj_ele_get_next_child(
    const ndobj_ele_t   *self,
    const ndobj_ele_t   *curr,
    const char          *name);
const ndobj_ele_t* ndobj_ele_get_prev_child(
    const ndobj_ele_t   *self,
    const ndobj_ele_t   *curr,
    const char          *name);

size_t ndobj_ele_encode(const ndobj_ele_t *self, void *buf, size_t bufsize);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
