#include <stdlib.h>
#include <string.h>
#include "ndobj_typedef.h"
#include "ndobj_core.h"
#include "ndobj_ele_string.h"

static const struct ndobj_ele_vtbl vtbl =
{
    .release = (void(*)(ndobj_ele_t*)) ndobj_ele_string_release,

    .get_name = (const char*(*)(const ndobj_ele_t*))    ndobj_ele_string_get_name,
    .get_type = (int(*)(const ndobj_ele_t*))            ndobj_ele_string_get_type,

    .get_string = (const char*(*)(const ndobj_ele_t*)) ndobj_ele_string_get_string,

    .encode = (size_t(*)(const ndobj_ele_t*,void*,size_t)) ndobj_ele_string_encode,
};

#ifdef _WIN32
static
char *strndup(const char *str, size_t size)
{
    size_t len = strlen(str);
    if( len > size ) len = size;

    char *buf = malloc( len + 1 );
    if( !buf ) return NULL;

    memcpy(buf, str, len);
    buf[len] = 0;

    return buf;
}
#endif

ndobj_ele_string_t* ndobj_ele_string_create(
    const char  *name,
    const char  *value,
    size_t      maxlen)
{
    ndobj_ele_string_t *inst = NULL;

    bool succ = false;
    do
    {
        inst = malloc(sizeof(*inst));
        if( !inst ) break;

        inst->super.vtbl = &vtbl;
        inst->super.prev = NULL;
        inst->super.next = NULL;

        inst->name  = strdup(name ? name : "");
        inst->value = strndup(value ? value : "", maxlen);
        if( !inst->name || !inst->value ) break;

        inst->len = strlen(inst->value);

        succ = true;
    } while(false);

    if( !succ && inst )
    {
        ndobj_ele_string_release(inst);
        inst = NULL;
    }

    return inst;
}

void ndobj_ele_string_release(ndobj_ele_string_t *self)
{
    if( self->value )
        free(self->value);

    if( self->name )
        free(self->name);

    free(self);
}

const char* ndobj_ele_string_get_name(const ndobj_ele_string_t *self)
{
    return self->name;
}

int ndobj_ele_string_get_type(const ndobj_ele_string_t *self)
{
    return NDOBJ_TYPE_STRING;
}

const char* ndobj_ele_string_get_string(const ndobj_ele_string_t *self)
{
    return self->value;
}

size_t ndobj_ele_string_encode(const ndobj_ele_string_t *self, void *buf, size_t bufsize)
{
    return
        ndobj_unit_encode(buf,
            bufsize,
            self->name,
            NDOBJ_TYPE_STRING,
            self->value,
            self->len);
}
