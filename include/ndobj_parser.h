/**
 * @file
 * @brief       Parse and create objects from binary data.
 * @author      王文佑
 * @date        2019-04-12
 * @copyright   ZLib Licence
 * @see         https://gitlab.com/iceignatius/ndobj
 */
#ifndef _NDOBJ_PARSER_H_
#define _NDOBJ_PARSER_H_

#include "ndobj_element.h"

#ifdef __cplusplus
extern "C" {
#endif

ndobj_ele_t* ndobj_parse(const void *data, size_t size);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
