#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "ndobj_intdat.h"
#include "ndobj_typedef.h"
#include "ndobj_core.h"
#include "ndobj_ele_unsigned.h"

static const struct ndobj_ele_vtbl vtbl =
{
    .release = (void(*)(ndobj_ele_t*))  ndobj_ele_unsigned_release,

    .get_name = (const char*(*)(const ndobj_ele_t*))    ndobj_ele_unsigned_get_name,
    .get_type = (int(*)(const ndobj_ele_t*))            ndobj_ele_unsigned_get_type,

    .get_unsigned = (unsigned(*)(const ndobj_ele_t*)) ndobj_ele_unsigned_get_unsigned,

    .encode = (size_t(*)(const ndobj_ele_t*,void*,size_t)) ndobj_ele_unsigned_encode,
};

ndobj_ele_unsigned_t* ndobj_ele_unsigned_create(const char *name, unsigned value)
{
    ndobj_ele_unsigned_t *inst = NULL;

    bool succ = false;
    do
    {
        inst = malloc(sizeof(*inst));
        if( !inst ) break;

        inst->super.vtbl = &vtbl;
        inst->super.prev = NULL;
        inst->super.next = NULL;

        inst->name  = strdup(name ? name : "");
        inst->value = value;
        if( !inst->name ) break;

        succ = true;
    } while(false);

    if( !succ && inst )
    {
        ndobj_ele_unsigned_release(inst);
        inst = NULL;
    }

    return inst;
}

ndobj_ele_unsigned_t* ndobj_ele_unsigned_create_import(
    const char *name,
    const void *bodydata,
    size_t bodysize)
{
    if( !bodydata || !bodysize ) return NULL;

    unsigned value;
    if( bodysize != ndobj_ele_unsigned_decode_body(bodydata, bodysize, &value) )
        return NULL;

    return ndobj_ele_unsigned_create(name, value);
}

void ndobj_ele_unsigned_release(ndobj_ele_unsigned_t *self)
{
    if( self->name )
        free(self->name);

    free(self);
}

const char* ndobj_ele_unsigned_get_name(const ndobj_ele_unsigned_t *self)
{
    return self->name;
}

int ndobj_ele_unsigned_get_type(const ndobj_ele_unsigned_t *self)
{
    return NDOBJ_TYPE_UNSIGNED;
}

unsigned ndobj_ele_unsigned_get_unsigned(const ndobj_ele_unsigned_t *self)
{
    return self->value;
}

size_t ndobj_ele_unsigned_encode(
    const ndobj_ele_unsigned_t *self,
    void    *buf,
    size_t  bufsize)
{
    uint8_t bodydata[8];
    size_t bodysize =
        ndobj_ele_unsigned_encode_body(
            buf ? bodydata : NULL,
            sizeof(bodydata),
            self->value);

    if( !bodysize )
        return 0;

    return
        ndobj_unit_encode(
            buf,
            bufsize,
            self->name,
            NDOBJ_TYPE_UNSIGNED,
            bodydata,
            bodysize);
}

static
size_t select_data_size(unsigned value)
{
    if( value <= UINT8_MAX )
        return 1;
    else if( value <= UINT16_MAX )
        return 2;
    else if( value <= UINT32_MAX )
        return 4;
    else
        return 8;
}

size_t ndobj_ele_unsigned_encode_body(void *buf, size_t bufsize, unsigned value)
{
    size_t size = select_data_size(value);
    if( !buf ) return size;
    if( bufsize < size ) return 0;

    switch( size )
    {
    case 1:
        ((uint8_t*)buf)[0] = value;
        break;

    case 2:
        ndobj_intdat_write_uint16be(buf, value);
        break;

    case 4:
        ndobj_intdat_write_uint32be(buf, value);
        break;

    case 8:
        ndobj_intdat_write_uint64be(buf, value);
        break;

    default:
        return 0;
    }

    return size;
}

size_t ndobj_ele_unsigned_decode_body(const void *src, size_t srcsize, unsigned *value)
{
    if( !src || !srcsize || !value ) return 0;

    switch( srcsize )
    {
    case 1:
        *value = ((const uint8_t*)src)[0];
        break;

    case 2:
        *value = ndobj_intdat_read_uint16be(src);
        break;

    case 4:
        *value = ndobj_intdat_read_uint32be(src);
        break;

    case 8:
        *value = ndobj_intdat_read_uint64be(src);
        break;

    default:
        return 0;
    }

    return srcsize;
}
