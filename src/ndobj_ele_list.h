#ifndef _NDOBJ_ELE_LIST_H_
#define _NDOBJ_ELE_LIST_H_

#include "ndobj_element.h"
#include "ndobj_element_struct.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct ndobj_ele_list
{
    ndobj_ele_t super;

    char *name;

    unsigned    count;
    ndobj_ele_t *head;
    ndobj_ele_t *tail;

} ndobj_ele_list_t;

ndobj_ele_list_t*   ndobj_ele_list_create(const char *name);
void                ndobj_ele_list_release(ndobj_ele_list_t *self);

const char* ndobj_ele_list_get_name(const ndobj_ele_list_t *self);
int         ndobj_ele_list_get_type(const ndobj_ele_list_t *self);

unsigned    ndobj_ele_list_count_children(const ndobj_ele_list_t *self);
bool        ndobj_ele_list_add_child(ndobj_ele_list_t *self, ndobj_ele_t *item);

const ndobj_ele_t* ndobj_ele_list_get_next_child(
    const ndobj_ele_list_t  *self,
    const ndobj_ele_t       *curr,
    const char              *name);
const ndobj_ele_t* ndobj_ele_list_get_prev_child(
    const ndobj_ele_list_t  *self,
    const ndobj_ele_t       *curr,
    const char              *name);

size_t ndobj_ele_list_encode(const ndobj_ele_list_t *self, void *buf, size_t bufsize);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
