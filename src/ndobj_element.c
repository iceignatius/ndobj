#include <assert.h>
#include "ndobj_typedef.h"
#include "ndobj_element_struct.h"
#include "ndobj_element.h"

void ndobj_ele_release(ndobj_ele_t *self)
{
    /**
     * @memberof ndobj_ele
     * @brief Not use the instance anymore.
     * @details Decrease the instance reference count,
     *          and may destroy the object instance and
     *          release all resources it hold
     *          if no one reference to it anymore.
     *
     * @param self Object instance.
     */
    if( atomic_fetch_sub(&self->refcnt, 1) == 1 )
    {
        assert( self->vtbl->release );
        self->vtbl->release(self);
    }
}

ndobj_ele_t* ndobj_ele_newref(ndobj_ele_t *self)
{
    /**
     * @memberof ndobj_ele
     * @brief Increase the reference count.
     *
     * @param self Object instance.
     * @return The object instance it self.
     */
    atomic_fetch_add(&self->refcnt, 1);
    return self;
}

const char* ndobj_ele_get_name(const ndobj_ele_t *self)
{
    /**
     * @memberof ndobj_ele
     * @brief Get object name.
     *
     * @param self Object instance.
     * @return The object name.
     */
    assert( self->vtbl->get_name );
    return self->vtbl->get_name(self);
}

int ndobj_ele_get_type(const ndobj_ele_t *self)
{
    /**
     * @memberof ndobj_ele
     * @brief Get object type.
     *
     * @param self Object instance.
     * @return The object type.
     */
    assert( self->vtbl->get_type );
    return self->vtbl->get_type(self);
}

bool ndobj_ele_is_binary(const ndobj_ele_t *self)
{
    /**
     * @memberof ndobj_ele
     * @brief Check if the object type is binary?
     *
     * @param self Object instance.
     * @return TRUE if it is the type; and FALSE otherwise.
     */
    return ndobj_ele_get_type(self) == NDOBJ_TYPE_BINARY;
}

bool ndobj_ele_is_string(const ndobj_ele_t *self)
{
    /**
     * @memberof ndobj_ele
     * @brief Check if the object type is string?
     *
     * @param self Object instance.
     * @return TRUE if it is the type; and FALSE otherwise.
     */
    return ndobj_ele_get_type(self) == NDOBJ_TYPE_STRING;
}

bool ndobj_ele_is_integer(const ndobj_ele_t *self)
{
    /**
     * @memberof ndobj_ele
     * @brief Check if the object type is signed integer?
     *
     * @param self Object instance.
     * @return TRUE if it is the type; and FALSE otherwise.
     */
    return ndobj_ele_get_type(self) == NDOBJ_TYPE_INTEGER;
}

bool ndobj_ele_is_unsigned(const ndobj_ele_t *self)
{
    /**
     * @memberof ndobj_ele
     * @brief Check if the object type is unsigned integer?
     *
     * @param self Object instance.
     * @return TRUE if it is the type; and FALSE otherwise.
     */
    return ndobj_ele_get_type(self) == NDOBJ_TYPE_UNSIGNED;
}

bool ndobj_ele_is_boolean(const ndobj_ele_t *self)
{
    /**
     * @memberof ndobj_ele
     * @brief Check if the object type is boolean?
     *
     * @param self Object instance.
     * @return TRUE if it is the type; and FALSE otherwise.
     */
    return ndobj_ele_get_type(self) == NDOBJ_TYPE_BOOLEAN;
}

bool ndobj_ele_is_float(const ndobj_ele_t *self)
{
    /**
     * @memberof ndobj_ele
     * @brief Check if the object type is floating point?
     *
     * @param self Object instance.
     * @return TRUE if it is the type; and FALSE otherwise.
     */
    return ndobj_ele_get_type(self) == NDOBJ_TYPE_FLOAT;
}

bool ndobj_ele_is_list(const ndobj_ele_t *self)
{
    /**
     * @memberof ndobj_ele
     * @brief Check if the object type is list container?
     *
     * @param self Object instance.
     * @return TRUE if it is the type; and FALSE otherwise.
     */
    return ndobj_ele_get_type(self) == NDOBJ_TYPE_LIST;
}

bool ndobj_ele_is_map(const ndobj_ele_t *self)
{
    /**
     * @memberof ndobj_ele
     * @brief Check if the object type is map container?
     *
     * @param self Object instance.
     * @return TRUE if it is the type; and FALSE otherwise.
     */
    return ndobj_ele_get_type(self) == NDOBJ_TYPE_MAP;
}

bool ndobj_ele_is_container(const ndobj_ele_t *self)
{
    /**
     * @memberof ndobj_ele
     * @brief Check if the object type is any kind of conatiner?
     *
     * @param self Object instance.
     * @return TRUE if it is the type; and FALSE otherwise.
     */
    int type = ndobj_ele_get_type(self);
    return type == NDOBJ_TYPE_LIST || type == NDOBJ_TYPE_MAP;
}

const void* ndobj_ele_get_bindata(const ndobj_ele_t *self)
{
    /**
     * @memberof ndobj_ele
     * @brief Get object payload data.
     *
     * @param self Object instance.
     * @return  The object payload data.
     *          If the payload size is ZERO, then
     *          the return value may be NULL or a valid address,
     *          it is depend on the implementation.
     *
     * @remarks The return value is undefined if the object type is not BINARY.
     */
    return self->vtbl->get_bindata ? self->vtbl->get_bindata(self) : NULL;
}

size_t ndobj_ele_get_binsize(const ndobj_ele_t *self)
{
    /**
     * @memberof ndobj_ele
     * @brief Get object payload size.
     *
     * @param self Object instance.
     * @return  The object payload size.
     *
     * @remarks The return value is undefined if the object type is not BINARY.
     */
    return self->vtbl->get_binsize ? self->vtbl->get_binsize(self) : 0;
}

const char* ndobj_ele_get_string(const ndobj_ele_t *self)
{
    /**
     * @memberof ndobj_ele
     * @brief Get object value in string format.
     *
     * @param self Object instance.
     * @return The string value; or NULL if the object type is not string.
     */
    return self->vtbl->get_string ? self->vtbl->get_string(self) : NULL;
}

int ndobj_ele_get_integer(const ndobj_ele_t *self)
{
    /**
     * @memberof ndobj_ele
     * @brief Get object value in signed integer format.
     *
     * @param self Object instance.
     * @return The integer value; or ZERO if the object type is not signed integer.
     */
    return self->vtbl->get_integer ? self->vtbl->get_integer(self) : 0;
}

unsigned ndobj_ele_get_unsigned(const ndobj_ele_t *self)
{
    /**
     * @memberof ndobj_ele
     * @brief Get object value in unsigned integer format.
     *
     * @param self Object instance.
     * @return The integer value; or ZERO if the object type is not unsigned integer.
     */
    return self->vtbl->get_unsigned ? self->vtbl->get_unsigned(self) : 0;
}

bool ndobj_ele_get_boolean(const ndobj_ele_t *self)
{
    /**
     * @memberof ndobj_ele
     * @brief Get object value in boolean format.
     *
     * @param self Object instance.
     * @return The boolean value; or FALSE if the object type is not boolean.
     */
    return self->vtbl->get_boolean ? self->vtbl->get_boolean(self) : false;
}

double ndobj_ele_get_float(const ndobj_ele_t *self)
{
    /**
     * @memberof ndobj_ele
     * @brief Get object value in floating point format.
     *
     * @param self Object instance.
     * @return  The floating point value; or
     *          ZERO if the object type is not floating point.
     */
    return self->vtbl->get_float ? self->vtbl->get_float(self) : 0;
}

unsigned ndobj_ele_count_children(const ndobj_ele_t *self)
{
    /**
     * @memberof ndobj_ele
     * @brief Count the number of children.
     *
     * @param self Object instance.
     * @return  The number of children; and
     *          ZERO means that the object have no child or
     *          the object is not a container.
     */
    return self->vtbl->count_children ? self->vtbl->count_children(self) : 0;
}

bool ndobj_ele_add_child(ndobj_ele_t *self, ndobj_ele_t *item)
{
    /**
     * @memberof ndobj_ele
     * @brief Set another object to be the child of self.
     *
     * @param self Object instance.
     * @param item The other object to be added.
     * @return TRUE if success; or FALSE if the object is not a container.
     */
    return self->vtbl->add_child ? self->vtbl->add_child(self, item) : false;
}

const ndobj_ele_t* ndobj_ele_get_first_child(
    const ndobj_ele_t   *self,
    const char          *name)
{
    /**
     * @memberof ndobj_ele
     * @brief Get the first child.
     *
     * @param self  Object instance.
     * @param name  Specify a name to search the first object which matched the name;
     *              or set NULL to accept any names.
     * @return  The first object that match the conditions; or
     *          NULL if no object be found, or the container is empty, or
     *          the object is not a container.
     */
    return self->vtbl->get_next_child ?
        self->vtbl->get_next_child(self, NULL, name) :
        NULL;
}

const ndobj_ele_t* ndobj_ele_get_last_child(
    const ndobj_ele_t   *self,
    const char          *name)
{
    /**
     * @memberof ndobj_ele
     * @brief Get the last child.
     *
     * @param self  Object instance.
     * @param name  Specify a name to search the last object which matched the name;
     *              or set NULL to accept any names.
     * @return  The last object that match the conditions; or
     *          NULL if no object be found, or the container is empty, or
     *          the object is not a container.
     */
    return self->vtbl->get_prev_child ?
        self->vtbl->get_prev_child(self, NULL, name) :
        NULL;
}

const ndobj_ele_t* ndobj_ele_get_next_child(
    const ndobj_ele_t   *self,
    const ndobj_ele_t   *curr,
    const char          *name)
{
    /**
     * @memberof ndobj_ele
     * @brief Get the next object.
     *
     * @param self  Object instance.
     * @param curr  The current child.
     *              This parameter can be NULL to search from head.
     * @param name  Specify a name to search the next object which matched the name;
     *              or set NULL to accept any names.
     * @return  The next object that match the conditions; or
     *          NULL if no object be found, or the container is empty, or
     *          the object is not a container.
     */
    return self->vtbl->get_next_child ?
        self->vtbl->get_next_child(self, curr, name) :
        NULL;
}

const ndobj_ele_t* ndobj_ele_get_prev_child(
    const ndobj_ele_t   *self,
    const ndobj_ele_t   *curr,
    const char          *name)
{
    /**
     * @memberof ndobj_ele
     * @brief Get the previous object.
     *
     * @param self  Object instance.
     * @param curr  The current child.
     *              This parameter can be NULL to search from tail.
     * @param name  Specify a name to search the previous object which matched the name;
     *              or set NULL to accept any names.
     * @return  The previous object that match the conditions; or
     *          NULL if no object be found, or the container is empty, or
     *          the object is not a container.
     */
    return self->vtbl->get_prev_child ?
        self->vtbl->get_prev_child(self, curr, name) :
        NULL;
}

size_t ndobj_ele_encode(const ndobj_ele_t *self, void *buf, size_t bufsize)
{
    /**
     * @memberof ndobj_ele
     * @brief Encode the object (recursively) to binary format data.
     *
     * @param self      Object instance.
     * @param buf       A buffer to receive the encoded data.
     *                  This parameter can be NULL to calculate the encoded size only.
     * @param bufsize   Size of the output buffer.
     * @retval >0   The number of bytes of data be filled to the output buffer;
     *              or the minimum size required of the output buffer if @a buf is NULL.
     * @retval ZERO Failed to encode data, ie. the output buffer is too small.
     */
    assert( self->vtbl->encode );
    return self->vtbl->encode(self, buf, bufsize);
}
