#ifndef _NDOBJ_ELE_INTEGER_H_
#define _NDOBJ_ELE_INTEGER_H_

#include "ndobj_element.h"
#include "ndobj_element_struct.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct ndobj_ele_integer
{
    ndobj_ele_t super;

    char    *name;
    int     value;

} ndobj_ele_integer_t;

ndobj_ele_integer_t* ndobj_ele_integer_create(const char *name, int value);
ndobj_ele_integer_t* ndobj_ele_integer_create_import(
    const char  *name,
    const void  *bodydata,
    size_t      bodysize);
void ndobj_ele_integer_release(ndobj_ele_integer_t *self);

const char* ndobj_ele_integer_get_name(const ndobj_ele_integer_t *self);
int         ndobj_ele_integer_get_type(const ndobj_ele_integer_t *self);

int ndobj_ele_integer_get_integer(const ndobj_ele_integer_t *self);

size_t ndobj_ele_integer_encode(
    const ndobj_ele_integer_t *self,
    void    *buf,
    size_t  bufsize);

size_t ndobj_ele_integer_encode_body(void *buf, size_t bufsize, int value);
size_t ndobj_ele_integer_decode_body(const void *src, size_t srcsize, int *value);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
