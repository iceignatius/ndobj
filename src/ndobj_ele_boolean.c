#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "ndobj_typedef.h"
#include "ndobj_core.h"
#include "ndobj_ele_boolean.h"

static const struct ndobj_ele_vtbl vtbl =
{
    .release = (void(*)(ndobj_ele_t*)) ndobj_ele_boolean_release,

    .get_name = (const char*(*)(const ndobj_ele_t*))    ndobj_ele_boolean_get_name,
    .get_type = (int(*)(const ndobj_ele_t*))            ndobj_ele_boolean_get_type,

    .get_boolean = (bool(*)(const ndobj_ele_t*)) ndobj_ele_boolean_get_boolean,

    .encode = (size_t(*)(const ndobj_ele_t*,void*,size_t)) ndobj_ele_boolean_encode,
};

ndobj_ele_boolean_t* ndobj_ele_boolean_create(const char *name, bool value)
{
    ndobj_ele_boolean_t *inst = NULL;

    bool succ = false;
    do
    {
        inst = malloc(sizeof(*inst));
        if( !inst ) break;

        ndobj_ele_init(&inst->super, &vtbl);

        inst->name  = strdup(name ? name : "");
        inst->value = value;
        if( !inst->name ) break;

        succ = true;
    } while(false);

    if( !succ && inst )
    {
        ndobj_ele_boolean_release(inst);
        inst = NULL;
    }

    return inst;
}

ndobj_ele_boolean_t* ndobj_ele_boolean_create_import(
    const char  *name,
    const void  *bodydata,
    size_t      bodysize)
{
    if( !bodydata || bodysize != 1 ) return NULL;

    return ndobj_ele_boolean_create(name, ((const uint8_t*)bodydata)[0]);
}

void ndobj_ele_boolean_release(ndobj_ele_boolean_t *self)
{
    if( self->name )
        free(self->name);

    free(self);
}

const char* ndobj_ele_boolean_get_name(const ndobj_ele_boolean_t *self)
{
    return self->name;
}

int ndobj_ele_boolean_get_type(const ndobj_ele_boolean_t *self)
{
    return NDOBJ_TYPE_BOOLEAN;
}

bool ndobj_ele_boolean_get_boolean(const ndobj_ele_boolean_t *self)
{
    return self->value;
}

size_t ndobj_ele_boolean_encode(
    const ndobj_ele_boolean_t *self,
    void    *buf,
    size_t  bufsize)
{
    uint8_t data = self->value ? 1 : 0;
    return
        ndobj_unit_encode(
            buf,
            bufsize,
            self->name,
            NDOBJ_TYPE_BOOLEAN,
            &data,
            sizeof(data));
}
