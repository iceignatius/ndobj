#include <stdlib.h>
#include <string.h>
#include "ndobj_typedef.h"
#include "ndobj_core.h"
#include "ndobj_ele_binary.h"

static const struct ndobj_ele_vtbl vtbl =
{
    .release = (void(*)(ndobj_ele_t*)) ndobj_ele_binary_release,

    .get_name = (const char*(*)(const ndobj_ele_t*))    ndobj_ele_binary_get_name,
    .get_type = (int(*)(const ndobj_ele_t*))            ndobj_ele_binary_get_type,

    .get_bindata = (const void*(*)(const ndobj_ele_t*)) ndobj_ele_binary_get_bindata,
    .get_binsize = (size_t(*)(const ndobj_ele_t*))      ndobj_ele_binary_get_binsize,

    .encode = (size_t(*)(const ndobj_ele_t*,void*,size_t)) ndobj_ele_binary_encode,
};

static
void* memdup(const void *src, size_t size)
{
    if( !src || !size ) return NULL;

    uint8_t *dst = malloc(size);
    if( dst )
        memcpy(dst, src, size);

    return dst;
}

ndobj_ele_binary_t* ndobj_ele_binary_create(
    const char  *name,
    const void  *data,
    size_t      size)
{
    ndobj_ele_binary_t *inst = NULL;

    bool succ = false;
    do
    {
        inst = malloc(sizeof(*inst));
        if( !inst ) break;

        ndobj_ele_init(&inst->super, &vtbl);

        inst->name = NULL;
        inst->data = NULL;
        inst->size = 0;

        inst->name = strdup(name ? name : "");
        if( !inst->name ) break;

        inst->size = data ? size : 0;
        inst->data = memdup(data, inst->size);
        if( inst->size && !inst->data ) break;

        succ = true;
    } while(false);

    if( !succ && inst )
    {
        ndobj_ele_binary_release(inst);
        inst = NULL;
    }

    return inst;
}

void ndobj_ele_binary_release(ndobj_ele_binary_t *self)
{
    if( self->data )
        free(self->data);

    if( self->name )
        free(self->name);

    free(self);
}

const char* ndobj_ele_binary_get_name(const ndobj_ele_binary_t *self)
{
    return self->name;
}

int ndobj_ele_binary_get_type(const ndobj_ele_binary_t *self)
{
    return NDOBJ_TYPE_BINARY;
}

const void* ndobj_ele_binary_get_bindata(const ndobj_ele_binary_t *self)
{
    return self->data;
}

size_t ndobj_ele_binary_get_binsize(const ndobj_ele_binary_t *self)
{
    return self->size;
}

size_t ndobj_ele_binary_encode(const ndobj_ele_binary_t *self, void *buf, size_t bufsize)
{
    return
        ndobj_unit_encode(
            buf,
            bufsize,
            self->name,
            NDOBJ_TYPE_BINARY,
            self->data,
            self->size);
}
