#ifndef _NDOBJ_INTDAT_H_
#define _NDOBJ_INTDAT_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

void ndobj_intdat_write_uint16be(uint8_t *buf, uint16_t value);
void ndobj_intdat_write_uint32be(uint8_t *buf, uint32_t value);
void ndobj_intdat_write_uint64be(uint8_t *buf, uint64_t value);

void ndobj_intdat_write_sint16be(uint8_t *buf, int16_t value);
void ndobj_intdat_write_sint32be(uint8_t *buf, int32_t value);
void ndobj_intdat_write_sint64be(uint8_t *buf, int64_t value);

uint16_t ndobj_intdat_read_uint16be(const uint8_t *src);
uint32_t ndobj_intdat_read_uint32be(const uint8_t *src);
uint64_t ndobj_intdat_read_uint64be(const uint8_t *src);

int16_t ndobj_intdat_read_sint16be(const uint8_t *src);
int32_t ndobj_intdat_read_sint32be(const uint8_t *src);
int64_t ndobj_intdat_read_sint64be(const uint8_t *src);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
